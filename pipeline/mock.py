import base64

import click
import faker

import dataset


fake = faker.Faker()


def generateMockPayment():
    """Generate a mock openpayments incoming payment"""
    return {
        "id": fake.uri(),
        "walletAddress": fake.uri(),
        "quoteId": fake.uri(),
        "failed": fake.pybool(),
        "receiver": fake.uri(),
        "debitAmount": {
            "value": str(fake.pyint()),
            "assetCode": fake.currency_code(),
            "assetScale": fake.pyint(),
        },
        "receiveAmount": {
            "value": str(fake.pyint()),
            "assetCode": fake.currency_code(),
            "assetScale": fake.pyint(),
        },
        "sentAmount": {
            "value": str(fake.pyint()),
            "assetCode": fake.currency_code(),
            "assetScale": fake.pyint(),
        },
        "metadata": {
            "description": fake.sentence(),
            "externalRef": fake.uuid4(),
        },
        "createdAt": fake.iso8601(),
        "updatedAt": fake.iso8601(),
    }


def generateMockDataset(size):
    """Generate a mock dataset for openpayments incoming requests"""

    payments = []
    for _ in range(size):
        payment = generateMockPayment()
        payment["fraud"] = fake.pybool()
        payments.append(payment)

    return dataset.processPaymentsArray(payments)


@click.command()
@click.option("--rows", help="Amount of rows to be generated", required=True, type=int)
@click.option("--csv-path", help="Path to store CSV", required=True)
def generateMockCSV(rows, csv_path):
    df = generateMockDataset(rows)
    df.to_csv(csv_path, index=False)


if __name__ == "__main__":
    generateMockCSV()
