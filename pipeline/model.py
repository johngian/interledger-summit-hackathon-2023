from ludwig.api import LudwigModel

DEFAULT_CONFIG_PATH = "./training-schema.yaml"


def train(configPath, df):
    model = LudwigModel(config=configPath)
    results = model.train(dataset=df)
    return results


def predict(modelPath, df):
    model = LudwigModel.load(modelPath)
    predictions = model.predict(df)
    return predictions
