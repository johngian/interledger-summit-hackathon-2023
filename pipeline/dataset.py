import pandas

from dateutil import parser as dateparser


def parseDateTime(dtstring):
    """Parse datetime objects"""
    return dateparser.isoparse(dtstring).timestamp()


def processPaymentsArray(payments):
    """
    Process payments from a paginated openpayments response
    and transform them to our flattened definition
    https://openpayments.guide/apis/resource-server/operations/list-outgoing-payments/
    """

    df = pandas.json_normalize(payments)
    df["receiveAmount.value"] = df["receiveAmount.value"].astype(int)
    df["sentAmount.value"] = df["sentAmount.value"].astype(int)
    df["debitAmount.value"] = df["debitAmount.value"].astype(int)
    df["createdAt"] = df["createdAt"].apply(parseDateTime)
    df["updatedAt"] = df["updatedAt"].apply(parseDateTime)

    return df
