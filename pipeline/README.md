# Quickstart
## Generate dataset
### Testing

#### Generate dataset
```
> python mock.py --rows 1000 --csv-path mock-payments-dataset.csv
```

#### Train model
```
> ludwig train --config training-schema.yaml --dataset mock-payments-dataset.csv
```

#### Serve model (via HTTP microservice) 

```
> ludwig serve --model_path results/experiment_run_0/model
```

#### Send a classification (prediction) request

```
> curl 127.0.0.1:8000 -X POST \
-F "id=https://ilp.rafiki.money/bob/outgoing-payments/3859b39e-4666-4ce5-8745-72f1864c5371" \
-F "walletAddress=https://ilp.rafiki.money/bob/" \
-F "failed=False" \
-F "receiver=https://ilp.rafiki.money/incoming-payments/2f1b0150-db73-49e8-8713-628baa4a17ff" \
-F "createdAt=2022-03-12T23:20:50.52Z" \
-F "updatedAt=2022-04-01T10:24:36.11Z" \
-F "debitAmount.value=2500" \
-F "debitAmount.assetCode=USD" \
-F "debitAmount.assetScale=2" \
-F "receiveAmount.value=2198" \
-F "receiveAmount.assetCode=EUR" \
-F "receiveAmount.assetScale=2" \
-F "sentAmount.value=1205" \
-F "sentAmount.assetCode=USD" \
-F "sentAmount.assetScale=2" \
-F "metadata.description=Thanks for the flowers!" \
-F "metadata.externalRef=INV-12876"
```