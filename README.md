# Interledger fraud detection

## Quickstart

### Prepare DB

```
> docker compose up -d db
> docker compose exec -it db psql -U dbuser app
app=# CREATE SCHEMA api;
app=# CREATE ROLE web_anon NOLOGIN;
app=# GRANT USAGE ON SCHEMA api TO web_anon;
app=# CREATE ROLE authenticator NOINHERIT NOCREATEDB NOCREATEROLE NOSUPERUSER LOGIN PASSWORD 'pass';
app=# GRANT web_anon TO authenticator;
```

### Start services

```
docker compose up -d rest swagger
```
